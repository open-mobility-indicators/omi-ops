- name: Install OS packages
  ansible.builtin.apt:
    install_recommends: no
    name:
      - docker.io
      - git
    state: present
    update_cache: yes

- name: Install gitlab-runner debian package # as no Debian bullseye package exists for now
  block:
    - name: Download Debian package
      ansible.builtin.get_url:
        url: https://gitlab-runner-downloads.s3.amazonaws.com/v14.4.0/deb/gitlab-runner_amd64.deb
        dest: /tmp/gitlab-runner_amd64.deb
        # checksum is found on https://gitlab-runner-downloads.s3.amazonaws.com/v14.4.0/index.html
        checksum: sha256:5611c852d414af4c99f64355e069a7af64b2f2289d13dd91a209521fef322fca

    - name: Install Debian package
      ansible.builtin.shell:
        cmd: dpkg -i /tmp/gitlab-runner_amd64.deb
        creates: /usr/bin/gitlab-runner

- name: Ensure that directories mounted as Docker volumes exist
  ansible.builtin.file:
    path: "{{ item }}"
    state: directory
    mode: "0755"
  loop:
    - "{{ mbtiles_dir }}"
    - "{{ indicator_data_dir }}"
    - "{{ workspaces_dir }}"

- name: Create Docker runner if it does not exist
  block:
    - name: Check if docker runner already exists
      import_tasks: check_runner_exists.yml
      vars:
        runner_executor: docker
        runner_name: "{{ gitlab_docker_runner_name }}"

    - name: Register a group-level docker runner
      ansible.builtin.command: >
        gitlab-runner register
          --non-interactive
          --url {{ gitlab_url | quote }}
          --registration-token {{ gitlab_registration_token | quote }}
          --env "container=docker"
          --executor "docker"
          --docker-image debian:latest
          --docker-volumes "{{ mbtiles_dir }}:/mbtiles:rw"
          --docker-volumes "{{ indicator_data_dir }}:/indicator_data:rw"
          --docker-volumes "{{ workspaces_dir }}:/workspaces:rw"
          --name {{ gitlab_docker_runner_name | quote }}
          --tag-list "build,compute-indicator,config,deploy,guard,omi-pipeline,process"
          --run-untagged="false"
          --locked="false"
          --access-level="not_protected"
          {% if register_paused_runners %}
          --paused
          {% endif %}
      become_user: root
      when: not runner_found
