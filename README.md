# Open Mobility Indicators – Ops

This repository contains resources used to deploy the Open Mobility Indicators project in production, using Ansible.

omi-ops is part of Open Mobility Indicators (see [project architecture](https://gitlab.com/open-mobility-indicators/website/-/wikis/4_Mainteneur/devops#quelle-est-larchitecture-de-la-plateforme-))

## Install pipeline server

This repository contains an Ansible playbook that installs GitLab Runner on a server, creates a runner for running the OMI pipeline, with the right Docker volumes, and installs the static file server and the tile server.

This playbook is intended for a server running Debian 11 (`bullseye`).

Requirements: install Ansible on your machine, following [Ansible documentation](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).

Retrieve the password of the Ansible vault from Jailbreak keepass file and write it to `ansible/vault/password`. This file is "git ignored" and must not be committed. Note: do not add a new line at the end of the file.

Then to run the Ansible playbook on an inventory:

```bash
ansible-playbook --inventory ansible/inventories/dedibox.yml --extra-vars @ansible/vault/variables.yml.enc --vault-password-file ansible/vault/password ansible/site.yml
```

To encrypt/decrypt the vault:

```bash

ansible-vault decrypt --vault-password-file ansible/vault/password ansible/vault/variables.yml.enc --output ansible/vault/variables.yml
ansible-vault encrypt --vault-password-file ansible/vault/password ansible/vault/variables.yml --output=ansible/vault/variables.yml.enc
```

## Update the version of the API

To deploy a new version of [omi-api](https://gitlab.com/open-mobility-indicators/omi-api), follow these steps.

First, update the `omi_api_container_image_version` variable defined in the [inventory file](ansible/inventories/dedibox.yml).

Then run the Ansible playbook as documented in the previous section.

If everything is OK, commit the inventory file.

To spare disk space on the server, remove the outdated versions of the omi-api Docker images, by logging to the server as `root`:

```bash
# Print all the versions of omi-api
docker image list | grep omi-api

# Delete all the outdated versions (replace {version} by the real value)
docker image rm registry.gitlab.com/open-mobility-indicators/omi-api:{version}

# Example:
docker image rm registry.gitlab.com/open-mobility-indicators/omi-api:0.2.0
docker image rm registry.gitlab.com/open-mobility-indicators/omi-api:0.2.1
```

## Remove unused Docker resources

Docker is used for 3 main purposes:

- to run the services of the OMI platform ([omi-api](https://gitlab.com/open-mobility-indicators/omi-api), mbtileserver, etc.) defined in a Docker Compose file
- to compute the indicators using GitLab Runner with a Docker executor
- to run the workspace cleaner script ([omi-workspace-cleaner](https://gitlab.com/open-mobility-indicators/omi-cleaner/-/tree/main/workspaces#omi-workspace-cleaner)) defined in a Docker Compose file

The Docker images of the OMI services are stored on the server.
We did not automate the deletion of the outdated images.
When an update is made, the DevOps should delete them as documented in the previous section.

However, the Ansible role named `docker_system_prune` installs a Systemd timer (i.e. like a cron job) to delete the outdated versions of the Docker images of the indicator projects that are run with GitLab Runner.

## Monitoring

The monitoring of the server has been made using [Grafana Cloud](https://grafana.com/products/cloud/) with the Linux Server Agent method (cf [docs](https://grafana.com/docs/grafana-cloud/quickstart/agent_linuxnode/)).

This is a hosted solution that only requires to install Grafana Agent to the server. The limits of the [free plan](https://grafana.com/products/cloud/pricing/) are enough for the project, for now.

The installation of the Grafana Agent was not wrapped into an Ansible role, but was done manually by following [this docs](https://grafana.com/docs/grafana-cloud/quickstart/agent_linuxnode/).

The Grafana Cloud space for OMI is <https://openmobilityindicators.grafana.net/>.

Email alerts can be configured [here](https://openmobilityindicators.grafana.net/alerting/notifications?alertmanager=grafana).

The dashboards are [here](https://openmobilityindicators.grafana.net/dashboards).
